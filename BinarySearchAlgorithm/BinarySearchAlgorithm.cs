﻿using System;
using System.Collections.Generic;

namespace Algorithms
{
    public static class BinarySearchAlgorithm
    {
        /// <summary>
        /// Implements binary search.
        /// </summary>
        /// <param name="reqiredElement">Element needs to be found.</param>
        /// <param name="array">Array for search.</param>
        /// <returns>
        /// The position of an element with a given value in sorted array.
        /// If element is not found returns -1.
        /// </returns>
        public static int FindIndex<TElement>(TElement reqiredElement, TElement[] array)
            where TElement : IComparable<TElement>
        {
            if(reqiredElement is null)
            {
                throw new ArgumentNullException(nameof(reqiredElement));
            }

            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (array.Length == 0)
            {
                return -1;
            }

            int leftBorder = 0, rightBorder = array.Length - 1, middleElementIndex;
            while (rightBorder - leftBorder >= 0)
            {
                middleElementIndex = leftBorder + ((rightBorder - leftBorder) / 2);

                int comparsionResult = array[middleElementIndex].CompareTo(reqiredElement);

                if (comparsionResult == 0)
                {
                    return middleElementIndex;
                }

                if (comparsionResult > 0)
                {
                    rightBorder = middleElementIndex - 1;
                }
                else
                {
                    leftBorder = middleElementIndex + 1;
                }
            }

            return -1;
        }

        /// <summary>
        /// Implements binary search.
        /// </summary>
        /// <param name="reqiredElement">Element needs to be found.</param>
        /// <param name="array">Array for search.</param>
        /// <param name="comparer">Comparer with comparsion logic for two elements.</param>
        /// <returns>
        /// The position of an element with a given value in sorted array.
        /// If element is not found returns -1.
        /// </returns>
        public static int FindIndex<TElement>(TElement reqiredElement, TElement[] array, IComparer<TElement> comparer)
        {
            if (reqiredElement is null)
            {
                throw new ArgumentNullException(nameof(reqiredElement));
            }

            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (comparer is null)
            {
                throw new ArgumentNullException(nameof(comparer));
            }

            if (array.Length == 0)
            {
                return -1;
            }

            int leftBorder = 0, rightBorder = array.Length - 1, middleElementIndex;
            while (rightBorder - leftBorder >= 0)
            {
                middleElementIndex = leftBorder + ((rightBorder - leftBorder) / 2);

                int comparsionResult = comparer.Compare(array[middleElementIndex], reqiredElement);

                if (comparsionResult == 0)
                {
                    return middleElementIndex;
                }

                if (comparsionResult > 0)
                {
                    rightBorder = middleElementIndex - 1;
                }
                else
                {
                    leftBorder = middleElementIndex + 1;
                }
            }

            return -1;
        }
    }
}
