﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Algorithms.Tests
{
    class StringLengthComparer : Comparer<string>
    {
        public override int Compare(string x, string y)
        {
            if(x.Length==y.Length)
            {
                return 0;
            }

            if(x.Length>y.Length)
            {
                return 1;
            }

            return -1;
        }
    }
}
