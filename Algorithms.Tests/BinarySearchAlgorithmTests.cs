using System;
using NUnit.Framework;

#pragma warning disable CA1707

namespace Algorithms.Tests
{
    public class BinarySearchAlgorithmTests
    {
        [TestCase(new[] { 6 }, 6, ExpectedResult = 0)]
        [TestCase(new[] { 1, 3, 4, 6, 8, 9, 11 }, 6, ExpectedResult = 3)]
        [TestCase(new[] { 1, 3, 4, 6, 8, 9, 11 }, 1, ExpectedResult = 0)]
        [TestCase(new[] { 1, 3, 4, 6, 8, 9, 11 }, 11, ExpectedResult = 6)]
        [TestCase(new[] { 1, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 634 }, 144, ExpectedResult = 9)]
        [TestCase(new[] { 1, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377 }, 21, ExpectedResult = 5)]
        public int BinarySearch_ReturnIndexOfValueInArray(int[] source, int value)
        {
            return BinarySearchAlgorithm.FindIndex(value, source);
        }

        [TestCase(new[] { 6 }, 7, ExpectedResult = -1)]
        [TestCase(new[] { 1, 3, 4, 6, 8, 9, 11 }, 0, ExpectedResult = -1)]
        [TestCase(new[] { 1, 3, 4, 6, 8, 9, 11 }, 14, ExpectedResult = -1)]
        [TestCase(new[] { 1, 3, 4, 6, 8, 9, 11 }, 11, ExpectedResult = 6)]
        [TestCase(new int[] { }, 144, ExpectedResult = -1)]
        [TestCase(new[] { 1, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377 }, 21, ExpectedResult = 5)]
        public int BinarySearch_ReturnMinusOne(int[] source, int value)
        {
            return BinarySearchAlgorithm.FindIndex(value, source);
        }

        [Test]
        public void BinarySearch_SourceArrayIsNull_ThrowArgumentNullException()
            => Assert.Throws<ArgumentNullException>(() => BinarySearchAlgorithm.FindIndex(10, null), "Source array cannot be null.");

        [TestCase(new[] { "","1","12","123","1234"}, "12", ExpectedResult = 2)]
        [TestCase(new[] { "", "1", "12", "1234" }, "1234", ExpectedResult = 3)]
        [TestCase(new[] { "", "1", "12", "1234" }, "", ExpectedResult = 0)]
        public int BinarySearch_ReturnIndexOfValueInArray(string[] source, string value)
        {
            var stringComparer = new StringLengthComparer();

            return BinarySearchAlgorithm.FindIndex(value, source, stringComparer);
        }

        [TestCase(new[] {  "1", "12", "1234" }, "", ExpectedResult = -1)]
        [TestCase(new[] { "", "1", "12", "123", "1234" }, "123456", ExpectedResult = -1)]
        public int BinarySearch_ReturnMinusOne(string[] source, string value)
        {
            var stringComparer = new StringLengthComparer();

            return BinarySearchAlgorithm.FindIndex(value, source, stringComparer);
        }


    }
}